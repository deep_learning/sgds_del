# Deep Learning Competence in Data Science Bachelor Program at FHNW  

This is the repository for the Deep Learning competence in the data science program at FHNW. It provides access to various different resources:

- Ressources that should be useful for learning deep learning.
- Exercises that should be useful for training your skills.
- Assignments that are expected to be elaborated to complete the course.    